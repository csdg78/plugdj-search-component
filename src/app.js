SearchModel = Backbone.Model.extend({
	initialize: function () {
		var results = {
		    "results": [
		        {
		            "name": "Genres",
		            "children": [
		                {"name": "Chillout", "value": "gender_chillout", "communities": 125}
					]
		        },
				{
		            "name": "Tags",
		            "children": [
		                {"name": "Chaff", "value": "tag_chaff", "communities": 25},
						{"name": "Chair", "value": "tag_", "communities": 5},
						{"name": "Choff", "value": "tag_", "communities": 3},
						{"name": "Cheer", "value": "tag_", "communities": 2},
						{"name": "Cheers", "value": "tag_", "communities": 2}
					]
		        },
				{
		            "name": "Languages",
		            "children": [
		                {"name": "Chinese", "value": "languages_chinese", "communities": 993}
					]
		        },
				{
		            "name": "Communities",
		            "children": [
		                {"name": "Chineseroom", "value": "chineseroom_id", "media": "http://placehold.it/50x50"},
						{"name": "Chamber of Fun", "value": "chamber_of_fun_id", "media": "http://placehold.it/50x50"},
						{"name": "Chinese room", "value": "chineseroom_id_2", "media": "http://placehold.it/50x50"},
						{"name": "ChamberofFun", "value": "chamber_of_fun_id_2", "media": "http://placehold.it/50x50"},
						{"name": "Chineseroom", "value": "chineseroom_id_3", "media": "http://placehold.it/50x50"}
					]
		        },
			]
		}

		for (var i in results) {
		    results[i].forEach(function(elem, index) {

				$('.autocomplete-search__target').append('<div class="target__content target__content--'+elem.name.toLowerCase()+'"><div class="genre__title">'+elem.name+'</div><ul class="genre__contents"></ul></div>');
				var sectionName = elem.name.toLowerCase();

		    	for (x = 0; x < elem.children.length; x++) {
		    		var communities = (elem.children[x].communities != undefined) ? elem.children[x].communities+' communities' : '';
		    		
		    		
		    		if(sectionName == 'communities') {
		    			console.log(elem.children[x].name)
		    			$('.target__content--'+sectionName+' ul').append('<li class="genre__contents--item"><span class="item__name"><img src="img/'+elem.children[x].name.toLowerCase().replace(/ /g, '-')+'.jpg" />'+elem.children[x].name+'</span><span class="item__communities">'+communities+'</span></li>');	
		    		} else {
		    			$('.target__content--'+sectionName+' ul').append('<li class="genre__contents--item"><span class="item__name">'+elem.children[x].name+'</span><span class="item__communities">'+communities+'</span></li>');
		    		}
			    }
		        
		    });
		}

		$('#search').on('keyup', function() {
			$('.autocomplete-search__input').addClass('loading');
			setTimeout(function(){
				$('.autocomplete-search__input').removeClass('loading');

				var inputVal = $('#search').val();
				
				if(inputVal == 'error') {
					$('.autocomplete-search__input').addClass('error');
					$('.autocomplete-search__target').removeClass('open');
					$('.autocomplete-search__input').removeClass('open');
				} else {				
					$('.autocomplete-search__target').addClass('open');
					$('.autocomplete-search__input').addClass('open');					
				}
			}, 2000);			
		})		        
    },   
});



$(document).ready(function(){
	var search = new SearchModel();	
});