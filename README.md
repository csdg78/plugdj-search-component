# plugdj-search-component

Plug.dj
UI Code Challenge

* Based on the mockup you got, create the search component using SASS preferably extending Bootstrap SASS.
Tip: You do not need to code in JS all the behaviors, as long as they can be reproduced by class states on the widget eg.: open, closed, loading, error etc...

Instructions:
Download the repository from bitbucket. Open the index.html file on a browser.
in the search Input write the word "error" to get the error message
in the search Input write the word "ch" to get the results.